use std::env;
use std::path::Path;
use std::io::{Read, Result};
use std::fs::{File, metadata};
use md5::{Md5, Digest};
use walkdir::WalkDir;
use glob::glob;


// Function to calculate and return the MD5 hash of a file
fn hash_file(path: &Path) -> Result<String> {
    // Try to open the file
    let mut file = match File::open(&path) {
        // If the file is opened successfully, use it
        Ok(file) => file,
        // If there's an error opening the file, print an error message and return the error
        Err(e) => {
            println!("Failed to open file {}: {}", path.display(), e);
            return Err(e);
        },
    };

    // Create a new MD5 hasher
    let mut hasher = Md5::new();
    // Create a buffer to store data from the file
    let mut buffer = [0; 8192];

    // Loop indefinitely, we'll break the loop when we've read all data from the file
    loop {
        match file.read(&mut buffer) {
            // If we've reached the end of the file (read returns 0), break the loop
            Ok(0) => break,
            // If we've read some data, update the hasher with it
            Ok(n) => hasher.update(&buffer[..n]),
            // If there's an error reading the file, print an error message and return the error
            Err(e) => {
                println!("Failed to read file {}: {}", path.display(), e);
                return Err(e);
            },
        };
    }

    // Finalize the hash computation and get the result
    let result = hasher.finalize();
    // Convert the hash result to a string and return it
    Ok(format!("{:x}", result))
}

// Main function where the program starts
fn main() {
    // Get the command-line arguments
    let args: Vec<String> = env::args().collect();

    // If there are less than 2 arguments, print an error message and exit
    if args.len() < 2 {
        println!("Please provide at least one file or directory as an argument");
        return;
    }

    // Iterate over each argument, skipping the first one (which is the name of the program itself)
    for path in args.iter().skip(1) {
        // Try to read the glob pattern
        for entry in glob(path).expect("Failed to read glob pattern") {
            // Process each entry that matches the glob pattern
            match entry {
                // If the path is valid, proceed
                Ok(path) => {
                    // Try to get the metadata of the path
                    let metadata = match metadata(&path) {
                        // If successful, proceed
                        Ok(metadata) => metadata,
                        // If there's an error getting the metadata, print an error message and continue with the next entry
                        Err(e) => {
                            println!("Failed to get metadata for {}: {}", path.display(), e);
                            continue;
                        },
                    };

                    // If the path is a file, try to calculate its hash
                    if metadata.is_file() {
                        match hash_file(&path) {
                            Ok(hash) => {
                                // If successful, print the hash
                                println!("{}  {}", hash, path.display());
                            }
                            Err(e) => {
                                // If there's an error calculating the hash, print an error message
                                eprintln!("Failed to hash file {}: {}", path.display(), e);
                            }
                        }
                    }
                    // If the path is a directory, iterate over each file in the directory
                    else if metadata.is_dir() {
                        for entry in WalkDir::new(&path) {
                            let entry = match entry {
                                // If the entry is a file, try to calculate its hash
                                Ok(entry) => entry,
                                // If there's an error accessing the path, print an error message and continue with the next entry
                                Err(e) => {
                                    println!("Failed to access path: {}", e);
                                    continue;
                                },
                            };
                            if entry.file_type().is_file() {
                                match hash_file(entry.path()) {
                                    Ok(hash) => {
                                        // If successful, print the hash
                                        println!("{}  {}", hash, entry.path().display());
                                    }
                                    Err(e) => {
                                        // If there's an error calculating the hash, print an error message
                                        eprintln!("Failed to hash file {}: {}", entry.path().display(), e);
                                    }
                                }
                            }
                        }
                    }
                },
                // If there's an error with the glob pattern, print an error message
                Err(e) => println!("{:?}", e),
            }
        }
    }
}

// Configuring the module for testing
#[cfg(test)]
mod tests {
    // Import the parent scope
    use super::*;
    // Import the Write trait from the std::io module for file writing operations using a buffered writer (BufWriter).
    use std::io::{BufWriter, Write};
    // Import the tempdir function from the tempfile crate for temporary directory handling
    use tempfile::tempdir;
    

    // Test to check if the function correctly hashes a string "Hello, world!"
    #[test]
    fn test_hash_file_hello_world() {
        // Create a temporary directory
        let dir = tempdir().unwrap();

        // Generate the file path within the temporary directory
        let file_path = dir.path().join("test.txt");

        // Create a new file at the specified path
        let mut file = File::create(&file_path).unwrap();

        // Write "Hello, world!" to the file
        writeln!(file, "Hello, world!").unwrap();

        // Hash the file using the `hash_file` function
        let hash = hash_file(&file_path).unwrap();

        // Assert that the computed hash matches the expected value
        assert_eq!(hash, "746308829575e17c3331bbcb00c0898b");
    }

    // Test to check if the function correctly hashes an empty file
    #[test]
    fn test_hash_empty_file() {
        // Create a temporary directory
        let dir = tempdir().unwrap();

        // Generate the file path within the temporary directory
        let file_path = dir.path().join("empty.txt");

        // Create an empty file at the specified path
        File::create(&file_path).unwrap();

        // Hash the file using the `hash_file` function
        let hash = hash_file(&file_path).unwrap();

        // Assert that the computed hash matches the expected value for an empty file
        assert_eq!(hash, "d41d8cd98f00b204e9800998ecf8427e");
    }

    // Test to check if the function correctly hashes a binary file
    #[test]
    fn test_hash_binary_file() {
        // Create a temporary directory
        let dir = tempdir().unwrap();

        // Generate the file path within the temporary directory
        let file_path = dir.path().join("test.bin");

        // Create a new file at the specified path
        let mut file = File::create(&file_path).unwrap();

        // Write binary data to the file
        file.write_all(&[0xff, 0x00, 0x0f]).unwrap();

        // Hash the file using the `hash_file` function
        let hash = hash_file(&file_path).unwrap();

        // Assert that the computed hash matches the expected value for the binary data
        assert_eq!(hash, "4614277c547bbf130efa9fb63e124d90");
    }

    #[test]
    #[cfg(windows)]
    #[should_panic(expected = "The system cannot find the file specified.")]
    fn test_hash_non_existent_file_windows() {
        test_non_existent_file();
    }
    
    #[test]
    #[cfg(not(windows))]
    #[should_panic(expected = "No such file or directory")]
    fn test_hash_non_existent_file_unix() {
        test_non_existent_file();
    }
    
    fn test_non_existent_file() {
        let dir = tempdir().unwrap();
        let file_path = dir.path().join("does_not_exist.txt");
        let _ = hash_file(&file_path).unwrap();
    }
    

    // Test to check if the function correctly hashes a large file ( 1GB )
    // This test is ignored by default because it could be slow and consume a lot of resources
    #[test]
    #[ignore]
    fn test_hash_large_file() {
        // Create a temporary directory
        let dir = tempdir().unwrap();
        
        // Generate the file path within the temporary directory
        let file_path = dir.path().join("big_test.txt");
        
        // Create a new file at the specified path
        let file = File::create(&file_path).unwrap();
        
        // Create a buffered writer to improve write performance
        let mut writer = BufWriter::new(file);
        
        // Write a large amount of data to the file
        (0..1_000_000_000).for_each(|_| {
            let mut buf = [0u8];
            writer.write_all(&mut buf).unwrap();
        });
        
        // Hash the file using the `hash_file` function
        let hash = hash_file(&file_path).unwrap();
        
        // Assert that the computed hash matches the expected value
        assert_eq!(hash, "feddce95e61869122cf88b266fe0acfa");
    }
    
}
