# File Hashing Tool: md5

<p align="center">
  <img src="https://img.shields.io/badge/Rust-1.64.0-green.svg" alt="Rustc Version 1.64.0">
  <img src="https://img.shields.io/badge/license-AGPLv3-blue.svg" alt="License: AGPLv3">
</p>

## Description

The `md5` tool is a command-line utility written in Rust for generating MD5 hash values of files and directories. It allows you to generate the MD5 hash of any file and recursively process directories to hash all contained files.

## Features

- Generate MD5 hash values for any given file.
- Recursively process directories to hash all contained files.
- Robust error handling - skips unreadable files and continues the operation.

## Installation

### Option 1: Download Pre-compiled Binary

You can download the latest version of `md5.exe` directly from our [GitHub Releases](https://gitlab.com/kostas_fournarakis/md5-rust/-/releases) page.

After downloading, move the `md5.exe` executable to `C:\Windows\System32`. This step requires administrative rights and allows the tool to be accessible from any terminal window:

```bash
move <download_path>\md5.exe C:\Windows\System32\
```
Replace `<download_path>` with the directory where you've downloaded the `md5.exe`.

### Option 2: Manual Build

First, ensure you have Rust and Cargo installed on your machine. If you don't, follow the instructions on the [official Rust website](https://www.rust-lang.org/tools/install).

Clone the repository and build the project with the following commands:

```bash
git clone https://gitlab.com/kostas_fournarakis/md5-rust.git
cd md5
cargo build --release
```

This will create a release build of the tool, which you'll find in the `target\release` directory as `md5.exe`.

To make the tool accessible from any terminal window, move the `md5.exe` executable to `C:\Windows\System32`. This step requires administrative rights:

```bash
move target\release\md5.exe C:\Windows\System32\
```

## Usage

To use the `md5` tool, navigate to the terminal and type:

```bash
md5 <filepath>
```

Replace `<filepath>` with the path to the file or directory you wish to generate an MD5 hash for.

For example, to hash all files in the current directory:

```bash
md5 *
```

If a directory path is provided, the tool will recursively process all files within it.

## Testing
This project has a comprehensive suite of tests to ensure its functionality. You can run these tests using the Rust's built-in testing framework with the following command:

```bash
cargo test
```
This will execute all unit tests in the project and display the results. The tests cover a variety of cases, including hashing different file types, handling of empty and non-existent files, and performance testing on large files.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

This project is licensed under the terms of the GNU General Public License (GPL) v3. See the [LICENSE](https://gitlab.com/kostas_fournarakis/md5-rust/-/blob/main/LICENSE) file for details.
